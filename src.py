import os
import selenium
import configparser
import pyodbc
import time
import random
import pyautogui
import subprocess


from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ChromeOptions
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

VMCONFIG_FILE_PATH = r'C:\\abc\\ABCMOBI.ini'

def getConfig(section, key, configfile):
    config = configparser.ConfigParser()
    config.read(configfile, encoding="utf-8")
    return config.get(section, key)
    

Driver = getConfig("database", "Driver", "config.ini")
SERVER = getConfig("database", "SERVER", "config.ini")
DATABASE = getConfig("database", "DATABASE", "config.ini")
UID = getConfig("database", "UID", "config.ini")
PWD = getConfig("database", "PWD", "config.ini")
yzmuser = getConfig("vcode", "username", "config.ini")
yzmpassword = getConfig("vcode", "password", "config.ini")
chromepath = getConfig("chrome", "chromedriverpath", "config.ini")
vmid = getConfig("VMInfo", "VMID", VMCONFIG_FILE_PATH)
internet_login = getConfig("internet", "login", "config.ini")
internet_pwd = getConfig("internet", "password", "config.ini")

# print(internet_login)
# print(internet_pwd)


pyautogui.FAILSAFE = False

class DbManager:
    def __init__(self):
        self.driver = Driver
        self.server = SERVER
        self.database = DATABASE
        self.uid = UID
        self.pwd = PWD
        self.conn = None
        self.cur = None

    # 连接数据库
    # connect to database
    def connectDatabase(self):
        co = "DRIVER={};SERVER={};DATABASE={};UID={};PWD={}".format('{' + self.driver + '}', self.server,
                                                                    self.database, self.uid, self.pwd)
        try:
            self.conn = pyodbc.connect(co)
            self.cur = self.conn.cursor()
            return True
        except:
            print("Error:Can't connect to database")
            return False

    # 关闭数据库
    # close db
    def close(self):
        # 如果数据打开，则关闭；否则没有操作
        # close connection to db
        if self.conn and self.cur:
            self.cur.close()
            self.conn.close()
        return True

    def execute(self, sql, params=None, commit=False, ):
        # 连接数据库
        # connect to db
        res = self.connectDatabase()
        if not res:
            return False
        try:
            if self.conn and self.cur:
                # 正常逻辑，执行sql，提交操作
                # if all good execute sql query
                rowcount = self.cur.execute(sql)
                self.conn.commit()
                
        except:
            print("execute failed: " + sql)
            print("params: " + str(params))
            self.close()
            return False
        return rowcount

    def fetchone(self, sql, params=None):
        res = self.execute(sql, params)
        if not res:
            print("查询失败")
            return False
        result = self.cur.fetchone()
        self.close()
        return result


class Browser:

    def __init__(self, byid, useragent):
        self.options = ChromeOptions()
        self.options.add_argument('log-level=3')
        self.options.add_experimental_option('excludeSwitches', ['enable-automation'])
        self.options.add_argument('user-agent={}'.format(useragent))
        self.options.add_argument(r"user-data-dir=" + "C:\\chromedata\\" + byid)
        # self.options.add_argument("--headless")
        # self.options.add_argument("--start-maximized")
        self.driver = webdriver.Chrome(chromepath, options=self.options)
        self.driver.maximize_window()
        self.wait = WebDriverWait(self.driver, 100)
        # self.driver.get(URL)

    def close(self):
        self.driver.close()

# get range of buyers which will be using in further processing
def get_buyer_range(db_manager, vmid):
    return db_manager.fetchone("SELECT KeywordFrom, KeywordTo FROM Control WHERE VMID='{}'".format(vmid))


# get aliexpress data from Buyer db
def get_alie_login_data(db_manager, buyer_id):
    return db_manager.fetchone("""SELECT buyer.AliexpressAccount, buyer.AliexpressPassword, buyer.BuyerID, buyer.BrowserUserAgent
     FROM Buyer buyer JOIN Control control ON buyer.BuyerID='{}'""".format(buyer_id))


def get_keywords_for_search(db_manager, vmid):
    return db_manager.fetchone("SELECT Keyword FROM Control WHERE VMID='{}'".format(vmid))

def get_browse_time_browse_page_and_browse_product_quantity(db_manager, vmid):
    return db_manager.fetchone("SELECT Browsetime, BrowsePage, BrowseOpenTotal FROM Control WHERE VMID='{}'".format(vmid))


# login to aliexpress account using data form db
def login_to_aliexpress(browser, alie_login, alie_pwd):
    browser.driver.get("https://login.aliexpress.com/")
    browser.driver.switch_to.frame("alibaba-login-box")
    var = 1
    while var < 100:
        try: 
            if browser.driver.find_element_by_xpath("//*[@id=\"login\"]/div[1]/div").get_attribute("class") != None:
                print("Already logined")
                browser.driver.get("https://ru.aliexpress.com/")
                break
        except:
            pass
        try:
            
            # 输入账号 / input login
            browser.driver.execute_script("document.querySelector('#fm-login-id').value='{}';".format(alie_login))
            # 输入密码 / input pwd
            browser.driver.execute_script("document.querySelector('#fm-login-password').value='{}';".format(alie_pwd))
            # click on login button
            browser.driver.find_element_by_class_name("fm-btn").find_element_by_class_name("fm-button").click()
        except:
            if browser.driver.current_url == "https://ru.aliexpress.com/":
                print("Login success")
                break
            else:
                continue
            

# search using received from db random querry, watch randomly generated pages and products according db data
def browse_alie_products(browser, keywords, browse_time, browse_page, browse_open_total):
    
    # browser.driver.find_element_by_class_name("search-key-box").send_keys("test")
    # time.sleep(3)
    browser.wait.until(EC.element_to_be_clickable((By.XPATH, "//*[@id=\"form-searchbar\"]/div[2]"))).click()
    browser.driver.execute_script("document.querySelector('#search-key').value='{}';".format(keywords[random.randrange(0,len(keywords)-1)])) # randomly choose search keyword
    browser.wait.until(EC.element_to_be_clickable((By.CLASS_NAME, "search-button"))).click()
    start_page = random.randint(int(browse_page[0]), int(browse_page[1]))
    end_page = random.randint(start_page, int(browse_page[1]))
    # print("********************************************")
    # print(start_page)
    # print(end_page)
    # print("********************************************")
    while start_page != end_page + 1:
        try:
            if browser.driver.find_element_by_class_name("ui-window-bd").get_attribute("class") != None:
                browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"we-wholesale-search-list\"]/div[18]/div/div/a"))).click() # close promotion block after login
        except:
            pass
        browser.wait.until(EC.element_to_be_clickable((By.XPATH, "//*[@id=\"pagination-bottom-input\"]"))).click()
        browser.driver.execute_script("document.querySelector('#pagination-bottom-input').value='{}';".format(start_page))
        browser.driver.execute_script("document.querySelector('#pagination-bottom-goto').click();")
        product_total_quantity_start = random.randint(int(browse_open_total[0]), int(browse_open_total[1]))
        product_total_quantity_end = random.randint(product_total_quantity_start, int(browse_open_total[1]))
        time.sleep(1)
        while product_total_quantity_start != product_total_quantity_end + 1:
            product_link = browser.wait.until(EC.visibility_of_element_located((By.ID, "hs-list-items"))).find_element_by_id("hs-below-list-items").find_element_by_xpath(
                "//*[@id=\"hs-below-list-items\"]/li[{}]/div/div[1]/div/a".format(random.randint(int(browse_open_total[0]), int(browse_open_total[1])))).get_attribute("href")
            # browser.wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "detail-bd-wrap")))
            browser.driver.execute_script("window.open('{}', 'new_window')".format(product_link))
            browser.driver.switch_to_window(browser.driver.window_handles[1])
            watch_time = random.randint(int(browse_time[0]), int(browse_time[1]))
            resolution = 0
            # print("********************************************")
            # print(product_total_quantity_start)
            # print(product_total_quantity_end)
            # print("********************************************")
            while resolution != 1920:
                resolution += 480
                browser.driver.execute_script("window.scrollTo(0, {})".format(resolution))
                # browser.actions.move_by_offset(-100, 100).perform()
                # ActionChains(browser.driver).move_by_offset(0, random.randint(0, 50)).perform()
                pyautogui.moveTo(random.randint(800, 980), random.randint(200, 350), random.randint(-1, 1))
                time.sleep(watch_time/4)
            browser.close()
            browser.driver.switch_to_window(browser.driver.window_handles[0])
            product_total_quantity_start += 1
            time.sleep(1)
        start_page += 1
    browser.close()
    print("One user done!")


def reset_intnet(se, internet_login, internet_pwd):
    cmd_connect_command_velcom = ['rasdial', 'Velcom PPPoE', '{}'.format(internet_login), '{}'.format(internet_pwd)]
    cmd_disconnect_command_velcom = ['rasdial', 'Velcom PPPoE', '/disconnect']
    subprocess.Popen(cmd_disconnect_command_velcom, stdout=subprocess.PIPE)
    time.sleep(1)
    subprocess.Popen(cmd_connect_command_velcom, stdout=subprocess.PIPE)



db_manager = DbManager()
start_buyer, end_buyer = get_buyer_range(db_manager, vmid)
keywords = str(get_keywords_for_search(db_manager, vmid)[0]).split(";")
browse_time, browse_page, browse_open_total = get_browse_time_browse_page_and_browse_product_quantity(db_manager, vmid)
# reset_intnet(1, internet_login, internet_pwd)
# print(browse_data)
# print(str(browse_time).split(";"))
# print(browse_page)
# print(browse_open_total)
# print(keywords)
for buyer_number in range(int(start_buyer), int(end_buyer) + 1):
    received_alie_login_data = get_alie_login_data(db_manager, buyer_number)
    browser = Browser(received_alie_login_data[2], received_alie_login_data[3])
    login_to_aliexpress(browser, str(received_alie_login_data[0]), str(received_alie_login_data[1]))
    try:
        if browser.driver.find_element_by_class_name("newuser-container").get_attribute("class") != None:
            browser.wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[8]/div/div/a"))).click() # close promotion block after login
        browse_alie_products(browser, keywords, str(browse_time).split(";"), str(browse_page).split(";"), str(browse_open_total).split(";"))
    except:
        browse_alie_products(browser, keywords, str(browse_time).split(";"), str(browse_page).split(";"), str(browse_open_total).split(";"))
    time.sleep(2)
print("All done!")
    